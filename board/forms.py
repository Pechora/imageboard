from django.forms import ModelForm, Textarea
from .models import Board, Post, file_size
from django import forms
from nocaptcha_recaptcha.fields import NoReCaptchaField
from django.core.exceptions import ValidationError

class PostForm(forms.ModelForm):
	captcha = NoReCaptchaField()
	class Meta:
		model = Post
		fields = ('title', 'comment', 'image')
		widgets = {
			'comment':Textarea(attrs={'id' : 'comment_area_id'}),
		}
