# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-25 12:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0003_auto_20160924_1816'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thread',
            name='board',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='post_comment',
            new_name='comment',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='post_title',
            new_name='title',
        ),
        migrations.RemoveField(
            model_name='post',
            name='thread',
        ),
        migrations.AddField(
            model_name='post',
            name='board',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='board.Board'),
        ),
        migrations.AddField(
            model_name='post',
            name='image',
            field=models.ImageField(blank=True, upload_to='images/'),
        ),
        migrations.AddField(
            model_name='post',
            name='last_post_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='post',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='board.Post'),
        ),
        migrations.AlterField(
            model_name='board',
            name='slug',
            field=models.SlugField(default='', max_length=15, unique=True),
        ),
        migrations.DeleteModel(
            name='Thread',
        ),
    ]
