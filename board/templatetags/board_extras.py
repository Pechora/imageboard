import re
from django import template
from django.utils.html import escape
from django.template.defaultfilters import linebreaksbr
from board.models import Post

register = template.Library()

@register.filter(name='get_posts')
def get_posts(thread):
	return Post.objects.filter(parent=thread).order_by('id')[:3]

@register.filter(name='post_links')
def post_links(value):
	value = linebreaksbr(value)
	return re.sub(r'&gt;&gt;(\d+)', lambda post_link: _link(post_link.group()), value)

def _link(value):
	post_id = value[len('&gt;&gt;'):]
	if post_id and post_id.isdigit():
		value = '<a href = #p'+str(post_id)+'>&gt;&gt;'+str(post_id)+'</a>'
		return value
	return linebreaksbr(value)

@register.filter(name='range_1_N') 
def range_1_N(num):
    return range(1, int(num) + 1)
