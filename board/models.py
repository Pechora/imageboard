from django.db import models
from django.utils import timezone
from django.db.models.signals import post_delete
from django.dispatch import receiver
import uuid
import os
from sorl.thumbnail import ImageField
from sorl.thumbnail import get_thumbnail
from django import template
from django.core.exceptions import ValidationError

def file_size(value):
    limit = 2 * 1024 * 1024
    if value.size > limit:
        raise ValidationError('File too large. Size should not exceed 2 MiB.')

register = template.Library()

from django.conf import settings
MEDIA_ROOT = settings.MEDIA_ROOT

class Board(models.Model):
	title = models.CharField(max_length=50, unique=True)
	text = models.TextField(max_length=2000)
	slug = models.SlugField(default='', max_length=15, unique=True)

	def __str__(self):
		return self.title


def handle_upload(instance, file):
	fn, fe = os.path.splitext(file)
	return os.path.join('images', str(uuid.uuid1())+fe)

class Post(models.Model):
	board = models.ForeignKey(Board, null=True, on_delete=models.CASCADE)
	parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='answers')
	title = models.CharField(max_length=200, default='')
	comment = models.TextField(max_length=2000, blank=False)
	image = models.ImageField(upload_to=handle_upload, blank=True, validators=[file_size])
	created_date = models.DateTimeField(default=timezone.now)
	last_post_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title

@receiver(post_delete, sender=Post)
def image_delete(sender, **kwargs):
	file = kwargs.get("instance")
	file.image.delete(save=False)
