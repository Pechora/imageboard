from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.main_page, name='home'),
    url(r'^(?P<post_id>\d+)/$', views.post_content, name="post_content"),
    url(r'^(?P<board_slug>\w+)/$', views.board_content, name='board_content'),
    url(r'^(?P<board_slug>\w+)/(?P<thread_id>\d+)/$', views.thread_content, name="thread_content"),
]
