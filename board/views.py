from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Board, Post
from .forms import PostForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def main_page(request):
	boards = Board.objects.all().order_by('title')
	return render(request, 'board/menu.html', {'boards' : boards})

def board_content(request, board_slug):
	cur_board = get_object_or_404(Board, slug=board_slug)
	if request.method == 'POST':
		form = PostForm(request.POST, request.FILES)
		if form.is_valid():
			thread = form.save(commit=False)
			print(thread.comment)
			thread.board = cur_board
			thread.parent = None
			thread.save()
			#print(form)


	boards = Board.objects.all().order_by('title')
	threads = Post.objects.filter(board = cur_board, parent =  None).order_by('-last_post_date')
	paginator = Paginator(threads, 10) # Show 25 threads per page
	page = request.GET.get('page')
	try:
		threads = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		threads = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		threads = paginator.page(paginator.num_pages)
	return render(request, 'board/board_list.html', {'boards' : boards, 'threads' : threads, 'form' : PostForm(), 'cur_board' : cur_board,})

def thread_content(request, board_slug, thread_id):
	cur_board = get_object_or_404(Board, slug=board_slug)
	cur_thread = get_object_or_404(Post, id=thread_id, board=cur_board)
	if request.method == 'POST':
		form = PostForm(request.POST, request.FILES)
		if form.is_valid():
			post = form.save(commit=False)
			post.board = cur_board
			post.parent = cur_thread
			post.save()
			cur_thread.last_post_date = post.created_date
			cur_thread.save()
	boards = Board.objects.all().order_by('title')
	posts = Post.objects.filter(parent=cur_thread).order_by('id')
	paginator = Paginator(posts, 20) # Show 25 posts per page
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		posts = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		posts = paginator.page(paginator.num_pages)
	return render(request, 'board/thread_list.html', {'boards' : boards, 'cur_board' : cur_board, 'cur_thread' : cur_thread, 'posts' : posts, 'form' : PostForm(),})


def post_content(request, post_id):
	post = get_object_or_404(Post, id=post_id)
	print(post_id)
	return render(request, 'board/post.html', {'post' : post})

